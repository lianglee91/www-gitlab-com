---
layout: markdown_page
title: "Involving experts workflow"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

When responding to community messages, you should always strive to involve a resident GitLab expert on the subject if possible.

This gives:

* a higher quality of answers
* shows that our whole company is committed to helping people
* the expert more feedback from users

## Workflow

Please ping the expert in the relevant channel (e.g. in `#frontend` if it's a frontend question) with:

```plain
@expert_username LINK: [LINK TO COMMUNITY COMMENT]
https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/#can-you-please-respond-to-this
Hello! Could you respond to this? Please answer on the social platform it was asked on originally (not Slack). If you don't know the answer, could you share your thoughts and pass it along to another expert? We're trying to make sure every comment gets a response. Thank you!
```

And add an internal note with the link of the Slack message to the associated Zendesk ticket. If there is no Zendesk ticket related to the mention (e.g. a HackerNews mention) track it in the `#community-relations` channel.

## Best practices

When trying to figure out who has expertise on what segment of the product, refer to the ["DevOps Stages"](/handbook/product/categories/#devops-stages) section of the [Product stages, groups, and categories](/handbook/product/categories/) page in the handbook. The [team page](/company/team/) can also be useful.

## Automation

TBD: Zendesk
