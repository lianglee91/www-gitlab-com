---
layout: markdown_page
title: "Chief of Staff Team"
description: "GitLab CoS Team Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

{::options parse_block_html="true" /}

----


## Quick Links and Fun Facts
* [Chief of Staff Job Family](/job-families/chief-executive-officer/chief-of-staff/)
* [Internal Strategy Consultant Job Family](/job-families/chief-executive-officer/internal-strategy-consultant/)
* [Project](https://gitlab.com/gitlab-com/cos-team/)
* CoS = Chief of Staff
* CoST = Chief of Staff Team
* [Current Milestone Issue Board](https://gitlab.com/gitlab-com/cos-team/-/boards/1337200?milestone_title=%23started&)


## Contact Us
* [chief-of-staff-team](https://gitlab.slack.com/archives/CN7MPDZF0/p1568035351000200) on Slack

## What projects does the CoST work on?

Chiefs of Staff and their teams usually work on three types of projects: 
projects that are important but not urgent or are under resourced, projects that are so broad that it can't live within a function but are too much work for the CEO, and projects that are important to the CEO. 

As GitLab grows, projects will come up that are important but are under resourced. 
Chiefs of Staff are known for their ability to become 80% effective on any subject quickly. 
They are generalists at their core and, while they bring special skills to the table, they are meant to be able to address important problems as they come up. 
A CoS might help source candidates for a strategic hire, fix grammatical errors in the handbook, and build a financial model all in the same day based on what is important or top of mind for the CEO at a given point. 
The goal of the CoS is not to do the work of other teams, but help address work that those teams may not have bandwidth to address but are important to the organization and/or the CEO.

Some projects or initiatives are very broad and cross-functional and *make sense* to belong to the CEO but are not a stregic use of the CEO's time. 
OKRs are a prime example. OKRs need to happen and are key to the business but it is not efficient for the CEO to shepherd the process along. 
The CoST is the shepherd for these sorts of projects and collaborates with all team members at GitLab to achieve success on such initatives. 

The CEO will have other projects that come up that he will task the CoST with, such as following up on something or carrying on a conversation on his behalf.

The CoST works closely with the [CEO](/job-families/chief-executive-officer/), the [E-Group](/handbook/leadership/#e-group), and the [EBA to the CEO](/job-families/people-ops/executive-business-administrator/).

## How to Work

The CoST works through numbered items on the CoS & Sid Google Sheet. 
It's format is structured like the [1-1 Suggested Agenda Format](/handbook/leadership/1-1/suggested-agenda-format/).
Many of the tasks on the sheet are quick asks- handbook MRs, formatting changes, or questions to be answered.
Small asks should be handled as quickly as possible. 
Other asks, such as OKR-related planning or an initiative that requires alignment with multiple stakeholders, requires forethought and more appropriate timing.
Some amount of time each week needs to be spent moving these sorts of tasks forward. 

As a rule, everything in the doc is a TODO for the CoST. 
At the beginning of the week, tasks that get TODO added to them are what are planned for that week. 
When they're DONE, they should be labelled as such. 
The CEO will review and delete the item once it's been assessed as completed. 

We work from the bottom up in agendas, unless told to prioritize otherwise. 

### Issue Board

In FY2020-Q3, we plan to move into an issue board, instead of a doc, to align with the organization's [dogfooding](/handbook/values/#dogfooding) value.
The CEO will drop a link in the doc and it will be on the members of the CoST to move the initative into an issue and link the issue in the doc.  
This will allow for an at-a-glance view of what the team is working on. 

### When to ping the CEO

Handbook changes that come through the doc can be converted to DONE in the doc with the MR linked if they are merged by the CoS or ISCs.
MRs related to the board, what is not public, or the [CEO's reponsibilities](/job-families/chief-executive-officer/) should be staged and merged by the CEO. 
If these occur in the doc, they become DOTOs, as they're action items for the CEO. 
Asks that come through Slack should have the MR linked in the appropriate thread, so that there is a closed loop in Slack. 

## Daily Standup

On the CoST, we use [Geekbot](https://geekbot.com) for our Daily Standups.
These are posted in #chief-of-staff-team-standups in Slack.  
Once team members are added to the daily standup list, they will receive a message from Geekbot via DM once they've been active on Slack after 6 AM in their local timezone. 
There is no pressure to respond to Geekbot as soon as it messages you. 
When Geekbot asks, "What will you do today?" try answering with specific details.
Give responses to Geekbot that truly communicate to your team what you're working on that day, so that your team can help you understand if some priority has shifted or there is additional context you may need.

## Performance Indicators

<iframe class="dashboard-embed" src="https://app.periscopedata.com/shared/bcfef667-7d2b-45d7-9638-a23e196e2067?embed=true" height="1200"> </iframe>

### Throughput

Thoughput for the CoS team is measured as all MRs in [across GitLab Company namespaces](https://gitlab.com/gitlab-data/analytics/tree/master/transform/snowflake-dbt/macros#get-internal-parent-namespaces) divided by the number of team members.

### Executive Time for the CEO

Executive Time is measured through the CEO calendar. The [Sid's Calendar Export](https://docs.google.com/spreadsheets/d/1xVaH7zrY8MIwI2TbZrcQnc8yepuHFFk87Ga7KZTB28o/edit?usp=drive_web&ouid=113483692538021736976) is a private google sheet built of a google apps script. 
The script is triggered to download all of the Calendar data every time it is run. 
The `filtered_columns` tab on this sheet pulls only the relevant columns for analyses. 
This tab is then pulled by [SheetLoad](/handbook/business-ops/data-team/#using-sheetload) into the SheetLoad drive and then ingested into [Snowflake](/handbook/business-ops/data-team/#-data-warehouse). 
Events [are categorized based on the event name and sender](https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/models/sheetload/base/sheetload_calendar.sql#L20), as outlined by the [EBA team best practices](/handbook/eba/#eba-team-best-practices).
All personal events are filtered out.
Those categories are applied to our [OKRs](/company/okrs/). 
Every event only gets one category. 
This is hard since some events may arguably fall into multiple. 
For example, a 1:1 with a CRO could go towards IACV, but since it's a 1:1 it goes towards Great Team, like all 1:1s.