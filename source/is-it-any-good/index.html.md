---
layout: markdown_page
title: Is it any good?
suppress_header: true
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

More than 100,000 organizations and millions of users depend on GitLab for their software development and operations.
[Over 2000 people contributed code to GitLab](http://contributors.gitlab.com/) (See our 2011 - 2018 [Feature Growth Chart](https://about.gitlab.com/2018/08/16/gitlab-ranked-44-on-inc-5000-list/)) and we've [liked thousands of tweets](https://twitter.com/gitlab/likes) from [our 50,000 followers](https://twitter.com/gitlab/followers).
The rest of this page contains some of the external acknowledgement GitLab has received.

## GitLab ranked number 4 software company (44th overall) on Inc. 5000 list of 2018's Fastest Growing Companies

[![GitLab ranked number 4th fastest-growing private software company on Inc. 5000 list of 2018's Fastest Growing Companies](/images/blogimages/inc-5000-2018-no4-software.png){: .small.right.wrap-text}](https://about.gitlab.com/2018/08/16/gitlab-ranked-44-on-inc-5000-list/)

GitLab is now America's [4th fastest-growing private software company](https://www.inc.com/inc5000/list/2018/industry/software), and 44th overall on this year's [Inc. 5000 list](/2018/08/16/gitlab-ranked-44-on-inc-5000-list/) with revenue growth of 6,213 percent over the past three years. This is the first year GitLab has appeared on Inc.'s 5000 list.

This year's Inc. 5000 ranking system is based on the percentage of revenue growth qualifying companies saw from 2014 to 2017. For consideration, companies needed to be private, for-profit, independent and U.S.-based as of December 31, 2017. The companies must have also been incorporated by March 31, 2014 with a minimum revenue of $200,000 for that year and $2 million for 2017.


## GitLab has 2/3 market share in the self-managed Git market

With more than 100,000 organizations self-hosting GitLab, we have the largest share of companies who choose to host their own code. We’re estimated to have two-thirds of the single tenant market. When [Bitrise surveyed](http://blog.bitrise.io/2017/01/27/state-of-app-development-in-2016.html#self-managed) ten thousand developers who build apps regularly on their platform, they found that 67 percent of self-managed apps prefer GitLab’s on-premise solution.

![Image via Bitrise blog](/images/blogimages/bitrise-self-hosted-chart.png){: .medium}<br>

Similarly, in their survey of roughly one thousand development teams, [BuddyBuild found](https://www.buddybuild.com/blog/source-code-hosting#selfhosted) that 79% of mobile developers who host their own code have chosen GitLab:

![Image via buddybuild blog](/images/blogimages/buddybuild-self-hosted-chart.png){: .medium}<br>

In their articles, both Bitrise and BuddyBuild note that few organizations use self-managed instances. We think there is a selection effect since both of them are SaaS-only offerings.

Based on our experience, the vast majority of enterprises ([organizations with over 5000 IT + TEDD employees](/handbook/sales/#market-segmentation)) self host their source code server (frequently on a cloud service like AWS or GCP) instead of using a SaaS service.

Another assumption is that git is the most popular version control technology for the enterprise. Certainly not every enterprise has switched (completely) yet but it does seem that git is [almost 10 times larger than SVN and Mercurial](https://trends.google.com/trends/explore?q=git,svn,perforce,mercurial,tfs).

## GitLab CI is the fastest growing CI/CD solution

Our commitment to seamless integration extends to CI. Integrated CI/CD is both more time and resource efficient than a set of distinct tools, and allows developers greater control over their build pipeline, so they can spot issues early and address them at a relatively low cost. Tighter integration between different stages of the development process makes it easier to cross-reference code, tests, and deployments while discussing them, allowing you to see the full context and iterate much more rapidly. We've heard from customers like [Ticketmaster](/2017/06/07/continous-integration-ticketmaster/) that adopting GitLab CI can transform the entire software development lifecycle (SDLC), in their case helping the Ticketmaster mobile development team deliver on the longstanding goal of weekly releases. As more and more companies look to embrace CI as part of their development methodology, having CI fully integrated into their overall SDLC solution will ensure these companies are able to realize the full potential of CI. You can read more about the benefits of integrated CI in our white paper, [Scaling Continuous Integration](/resources/whitepaper-scaled-ci-cd/).

In his post on [building Heroku CI](https://blog.heroku.com/building-tools-for-developers-heroku-ci), Heroku’s Ike DeLorenzo noted that GitLab CI is “clearly the biggest mover in activity on Stack Overflow,” with more popularity than both Travis CI and CircleCI.

GitLab is the second most popular CI system in [a report on cloud trends from Digital Ocean](https://assets.digitalocean.com/currents-report/DigitalOcean-Currents-Q4-2017.pdf).

![Image via Digital Ocean](/images/ci/gitlab-popular-ci.png){: .medium}

## GitLab CI is a leader in the The Forrester Wave™

![Forrester Wave graphic](/images/home/forrester-ci-wave-graphic.svg){: .small .margin-top20 .margin-bottom20}

[ Forrester has evaluated GitLab as a Leader in Continuous Integration in The Forrester Wave™: Continuous Integration Tools, Q3 2017 report.](/resources/forrester-wave-ci-2017/)

## GitLab is a top 3 innovator in IDC's list of Agile Code Development Technologies for 2018

![IDC innovators banner](/images/logos/idc-innovators-banner.png){: .margin-top20 .margin-bottom20}

[IDC recognized GitLab as a top 3 innovator in Agile Code Development Technologies for 2018.](https://about.gitlab.com/resources/report-idc-innovators-agile-code-development-2018/)

## GitLab is a strong performer in the new Forrester Value Stream Management Tools 2018 Wave Report

![Forrester VSM Wave graphic](/images/home/forrester-vsm-graphic.png){: .small .margin-top20 .margin-bottom20}

[Forrester has evaluated GitLab as a strong performer for VSM capabilities on top of its end to end DevOps capabilities.](https://about.gitlab.com/resources/forrester-new-wave-vsm-2018/)

## GitLab is one of the top 30 open source projects

[GitLab is one of the 30 Highest Velocity Open Source Projects](/2017/07/06/gitlab-top-30-highest-velocity-open-source/).

## Our customers love sharing their successes

GitLab customers are often asked to speak at events. [Hear their stories and learn from their experiences](https://about.gitlab.com/customers/marketplace/). 

## GitLab has more than 2,000 contributors

[GitLab contributors list](http://contributors.gitlab.com/).

## GitLab has been voted as G2 Crowd Leader in 2018

![G2 gitlab image](/images/logos/gitlab-g2.svg){: .small .margin-top20 .margin-bottom20}

[GitLab is a G2 Crowd Leader based on user reviews](https://www.g2crowd.com/products/gitlab/reviews)

## GitLab ranked above GitHub as a top developer tool

The results of the [Axosoft 2019 survey](https://blog.axosoft.com/top-developer-tools-2019/) of over 1,000 software engineers from elite organizations across the world ranked GitLab in the top 20 developer tools. Their blog states that: "GitLab is giving GitHub a run for its money! GitLab climbed the ranks 4 spots and overtook GitHub for the first year. While GitHub had the most significant drop in rankings from number 3 to number 11 year-over-year.".

[![GitLab ranked above GitHub in Axosoft top 20 Dev Tools for 2019](/images/blogimages/axosoft-top-20-devtools-2019.png){: .small .margin-top20 .margin-bottom20}](https://blog.axosoft.com/top-developer-tools-2019/)

## Why is this page called 'is it any good?'

When people first hear about a new product they frequently ask if it is any good. A Hacker News user [remarked](https://news.ycombinator.com/item?id=3067434): 'Note to self: Starting immediately, all raganwald projects will have a “Is it any good?” section in the readme, and the answer shall be “yes."'. We took inspiration from that and added it to the [GitLab readme](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/README.md#is-it-any-good). This page continues that tradition.
